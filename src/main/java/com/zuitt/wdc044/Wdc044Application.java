package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


//This file serves as our entry point

//@SpringBootApplication this is called as "Annotation" Mark represented by the @ symbol
//This is a shortcut or method we manually create or describe our framework
//Annotations are used to provide supplemental information about the program

@SpringBootApplication

@RestController

//@RequestParam

public class Wdc044Application {

	//This method starts the whole Springboot Framework

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")

	public String hello(@RequestParam(value="name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value="greet", defaultValue = "World") String greet){
		return String.format("Good evening, %s! Welcome to Batch 293!", greet);
	}

	//Activity 1
	@GetMapping("/hi")
	public String hi(@RequestParam(value="user", defaultValue = "user") String user){
		return String.format("hi %s!", user);
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="user", defaultValue = "user") String user){
		return String.format("Hello %s! Your age is ", user);
	}
}
