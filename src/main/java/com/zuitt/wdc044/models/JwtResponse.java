package com.zuitt.wdc044.models;

import java.io.Serializable;

//This is the model/class of the object when sending our generated JWT as response
//From the AuthController File
public class JwtResponse implements Serializable {

    //JwtResponse create model for token response
    private static final long serialVersionUID = 7566815519555087892L;

    private final String jwtToken;

    public JwtResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}

